﻿using Microsoft.Win32;
using System;

namespace _3matic.TestHelpers
{
    public static class RegistryHelper
    {
        public static string GetProductCode(string name)
        {
            var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default);
            var productCode = GetProductCode(hklm, name);

            if (string.IsNullOrEmpty(productCode))
            {
                hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                productCode = GetProductCode(hklm, name);

                //RegistryView.Registry32 not required in this context
            }

            return productCode;
        }

        public static string GetProductVersion(string code)
        {
            var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default);
            var productVersion = GetProductVersion(hklm, code);

            if (string.IsNullOrEmpty(productVersion))
            {
                hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                productVersion = GetProductVersion(hklm, code);

                //RegistryView.Registry32 not required in this context
            }

            return productVersion;
        }

        #region Private Methods

        private static string GetProductCode(RegistryKey hklm, string name)
        {
            string productCode = "";

            var key = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
            foreach (var keyName in key.GetSubKeyNames())
            {
                var subKey = key.OpenSubKey(keyName);
                var displayName = subKey.GetValue("DisplayName") as string;
                if (displayName != null && displayName.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                {
                    productCode = keyName;
                    break;
                }
            }

            return productCode;
        }

        private static string GetProductVersion(RegistryKey hklm, string code)
        {
            string productVersion = "";

            var subKey = hklm.OpenSubKey(string.Format(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{0}", code));
            if (subKey != null)
            {
                productVersion = subKey.GetValue("DisplayVersion") as string;
            }

            return productVersion;
        }

        #endregion
    }
}
