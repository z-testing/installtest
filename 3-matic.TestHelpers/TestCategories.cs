﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3matic.TestHelpers
{
    /// <summary>
    /// A class containing test category names.  This class cannot be inherited.
    /// </summary>
    public static class TestCategories
    {
        /// <summary>
        /// The test category name for ACE Product tests.
        /// </summary>
        public const string ACE = "ACE";

		/// <summary>
		/// The test category name for component tests.
		/// </summary>
		public const string Component = "Component";

        /// <summary>
        /// The test category name for data-driven tests.
        /// </summary>
        public const string DataDriven = "DataDriven";

        /// <summary>
        /// The test category name for high priority tests.
        /// </summary>
        public const string HighPriority = "HighPriority";

        /// <summary>
        /// The test category name for Integration tests.
        /// </summary>
        public const string Integration = "Integration";

        /// <summary>
        /// The test category name for load tests.
        /// </summary>
        public const string Load = "Load";

        /// <summary>
        /// The test category name for tests that should only be run locally.
        /// </summary>
        public const string LocalOnly = "Local-Only";

        /// <summary>
        /// The test category name for long-running tests.
        /// </summary>
        public const string LongRunning = "Long-Running";

        /// <summary>
        /// The test category name for low priority tests.
        /// </summary>
        public const string LowPriority = "LowPriority";

        /// <summary>
        /// The test category name for performance tests.
        /// </summary>
        public const string Performance = "Performance";

        /// <summary>
        /// The test category name for post-deployment tests.
        /// </summary>
        public const string PostDeployment = "Post-Deployment";

        /// <summary>
        /// The test category name for regression tests for bugs.
        /// </summary>
        public const string Regression = "Regression";

        /// <summary>
        /// The name of the test category for test that require administrative permissions to run.
        /// </summary>
        public const string RequiresAdministrativePermissions = "RequiresAdministrativePermissions";

        /// <summary>
        /// The test category name for Setup tests.
        /// </summary>
        public const string Setup = "Setup";

        /// <summary>
        /// The test category name for smoke tests.
        /// </summary>
        public const string Smoke = "Smoke";

        /// <summary>
        /// The test category name for tests that have not yet been implemented.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Todo", Justification = "Capitals are used by convention.")]
        public const string Todo = "TODO";

        /// <summary>
        /// The test category name for UI tests.
        /// </summary>
        public const string UI = "UI";
    }
}
