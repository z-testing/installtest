﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Security.Principal;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _3matic.TestHelpers
{
    public static class TestSetupHelpers
    {
        /// <summary>
        /// Ensures that the current user has administrator privileges.
        /// </summary>
        /// <remarks>
        /// Any test calling this method is marked as Inconclusive if
        /// the current user does not have administrative privileges.
        /// </remarks>
        public static void EnsureCurrentUserIsAdmin()
        {
            using (WindowsIdentity identity = WindowsIdentity.GetCurrent())
            {
                if (!new WindowsPrincipal(identity).IsInRole(WindowsBuiltInRole.Administrator))
                {
                    Assert.Inconclusive("The current user '{0}' does not have Administrator privileges.", identity.Name);
                }
            }
        }

        /// <summary>
        /// Ensures that the current domain is an internal MATONE domain.
        /// </summary>
        /// <remarks>
        /// Any test calling this method is marked as Inconclusive if
        /// the current user does not have administrative privileges.
        /// </remarks>
        /// <summary>
        /// Marks the current test as inconclusive if run from outside the ACE network.
        /// </summary>
        public static void EnsureMachineOnInternalNetwork()
        {
            if (!Environment.UserDomainName.StartsWith("ACE", StringComparison.OrdinalIgnoreCase))
            {
                Assert.Inconclusive("This test can only be run on the Matone networks.");
            }
        }
    }
}
