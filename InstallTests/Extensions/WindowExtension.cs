﻿using InstallTests.UtilObjects;
using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Automation;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace InstallTests.Extensions
{
    public static class WindowExtension
    {
        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool SetForegroundWindow(IntPtr windowHandle);

        public static Window WaitForWindow(this Application application, Func<Window, bool> predicate)
        {
            Window expectedWindow = null;
            var windowCheck = false;
            var startWaitingTime = DateTime.Now;
            var timeout = ConfigurationHelper.GetWaitForWindowTimeout();

            do
            {
                Thread.Sleep(1000);
                try
                {
                    var windows = application.GetWindows();
                    if (windows.Count > 0)
                    {
                        var window = windows.First();
                        window.WaitWhileBusy();
                        windowCheck = predicate(window);
                        if (windowCheck)
                        {
                            expectedWindow = window;
                        }
                    }
                }
                catch (ElementNotAvailableException)
                {
                    //window no longer available
                }

                var timeSpan = DateTime.Now - startWaitingTime;
                if (timeSpan.TotalMilliseconds > timeout)
                {
                    windowCheck = true;
                }

            } while (!windowCheck);

            if (expectedWindow == null)
                throw new NullReferenceException(string.Format("Window not found after waited for {0} milliseconds", timeout));

            return expectedWindow;
        }

        public static void WaitForButtonAndClick(this Application application, SearchCriteria searchCriteria, string screenshot = "")
        {
            var window = application.WaitForWindow((win) =>
            {
                var found = false;
                found = win.Exists<Button>(searchCriteria);
                if (found)
                {
                    var item = win.Get<Button>(searchCriteria);
                    found = item.Enabled;
                }
                return found;
            });
            if (!window.IsCurrentlyActive)
            {
                SetForegroundWindow(application.Process.MainWindowHandle);
            }
            var button = window.Get<Button>(searchCriteria);
            if (!string.IsNullOrEmpty(screenshot))
            {
                window.TakeScreenshot(screenshot);
            }
            button.Click();
        }

        public static bool GetButtonAndClick(this Window window, string buttonName, string screenshot = "")
        {
            if (window.Exists<Button>(SearchCriteria.ByText(buttonName)))
            {
                var button = window.Get<Button>(SearchCriteria.ByText(buttonName));
                if (!string.IsNullOrEmpty(screenshot))
                {
                    window.TakeScreenshot(screenshot);
                }
                button.Click();
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void TakeScreenshot(this Window window, string appendTitle, int waitMilliseconds = 1000)
        {
            var rect = window.Bounds;
            ScreenshotUtil.Instance.TakeScreenshot(rect, appendTitle, waitMilliseconds);
        }

        public static void TakeWindowsScreenshot(this Application application, string appendTitle, int waitMilliseconds = 1000)
        {
            var bounds = application.GetWindows().Select(win => win.Bounds);
            var rect = Rect.Empty;
            foreach (var bound in bounds)
            {
                rect.Union(bound);
            }

            if (!rect.IsEmpty)
            {
                ScreenshotUtil.Instance.TakeScreenshot(rect, appendTitle, waitMilliseconds);
            }
        }
    }
}
