using InstallTests.Extensions;
using InstallTests.UtilObjects;
using InstallTests.WindowObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using TestStack.White;
using TestStack.White.Configuration;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;

namespace InstallTests
{
    public abstract class TestBase
    {
        static string ApplicationPath = string.Empty;
        //static string ApplicationProcessName = "Setup";
        //static string ChromeProcessName = "chrome";
        static string MSIProcessName = "msiexec";
        public Application app;

        [TestInitialize]
        public void Setup()
        {
            KillRunningApplication("CrashSender1402");
            KillRunningApplication(MSIProcessName);
            CoreAppXmlConfiguration.Instance.BusyTimeout = ConfigurationHelper.GetWaitForWindowTimeout();
        }

        [TestCleanup]
        public void Clean()
        {
            TryToFinishMSIExecution();

            KillRunningApplication(MSIProcessName);
            Thread.Sleep(2000);

            if (app != null && app.Process != null && !app.Process.HasExited)
            {
                KillRunningApplication(app.Process.ProcessName);
            }
            //KillRunningApplication(ChromeProcessName);
            //Thread.Sleep(2000);
        }

        #region Method

        private void KillRunningApplication(string applicationName)
        {
            Process[] processes = Process.GetProcessesByName(applicationName);
            foreach (Process process in processes)
            {
                if (!process.HasExited)
                {
                    process.Kill();
                }
            }
        }

        private void TryToFinishMSIExecution()
        {
            var processes = Process.GetProcessesByName(MSIProcessName);
            foreach (Process process in processes)
            {
                if (!process.HasExited)
                {
                    try
                    {
                        var application = Application.Attach(process.Id);
                        if (application.GetWindows().Count > 0)
                        {
                            var buttonNames = new List<string>
                            {
                                "Cancel",
                                "OK",
                                "Finish"
                            };

                            var buttonsToClick = new Queue<SearchCriteria>(buttonNames.Select(name => SearchCriteria.ByText(name)));
                            while (buttonsToClick.Count > 0)
                            {
                                var criteria = buttonsToClick.Dequeue();
                                application.WaitForButtonAndClick(criteria);
                            }
                            application.Close();
                        }
                    }
                    catch { }
                }
            }
        }

        public void InitializeApplication(string applicationTag)
        {
            ApplicationPath = ConfigurationHelper.GetProductInstallationPath(applicationTag);

            var pStart = new ProcessStartInfo
            {
                FileName = ApplicationPath,
                Arguments = "",
                CreateNoWindow = true,
                UseShellExecute = false,
                Verb = "runas",
            };
            
            app = Application.Launch(pStart);        
        }

        protected virtual void Init(string productType, int waitPeriod, bool needScreenshot = true, string screenshot = "")
        {
            InitializeApplication(productType);
            if (needScreenshot)
            {
                app.TakeWindowsScreenshot(string.Format("ExtractingPackage{0}", screenshot));
            }
            Windows.Init(app);
            var window = app.WaitForWindow((win) =>
            {
                return !win.Exists<ProgressBar>();
            });
        }

        #endregion
    }
}
