﻿using _3matic.TestHelpers;
using FluentAssertions;
using InstallTests.Extensions;
using InstallTests.UtilObjects;
using InstallTests.WindowObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace InstallTests.TestCase
{
    // Complete
    [TestClass]
    public class CancelInstallation : TestBase
    {
        #region Part4 Cancelling Installation
        [TestCategory(TestCategories.ACE)]
        [TestMethod]
        public void Setup_3matiACE_CancelSetup()
        {
            Init("ACE", 40);

            Windows.WelcomeWindow
                    .CancelSetup()
                    .ClickYes()
                    .ClickFinish();

            Windows.CancelOkSetupWindow
                .IsButtonCancelOkExists().Should().BeTrue();

            Windows.CancelOkSetupWindow.ClickCancelOK();
        }

        public void Setup_3matic_CancelSetup_AfterInstallButton()
        {
            Init("BME", 30000);

            Windows.WelcomeWindow
                            .GotoEndUserAgreement()
                            .AcceptTermsOfUsage()
                            .GotoChooseSetupType()
                            .SelectCompleteInstall()
                            .IsButtonInstallExists().Should().BeTrue();

            Windows.WelcomeWindow
                    .CancelSetup()
                    .ClickYes()
                    .ClickFinish();

            Windows.CancelOkSetupWindow.ClickCancelOK();
        }

        [TestCategory(TestCategories.ACE)]
        [TestMethod]
        [ScreenshotFolder("CancelInstallation_ACE")]
        public void CancelInstallation_of_3maticACE_Recursively()
        {
            //arrange
            ReinstallationUtil.TryToUninstall3maticIfInstalled(TestCategories.ACE);

            //act & assert
            CancelInstallation_of_3matic(TestCategories.ACE, 4);
        }

        [TestCategory(TestCategories.ACE)]
        [TestCategory(TestCategories.Regression)]
        [TestMethod]
        public void CancelInstallation_of_3maticACE_CancelNo_CancelYes()
        {
            //arrange
            ReinstallationUtil.TryToUninstall3maticIfInstalled(TestCategories.ACE);

            //act & assert
            CancelInstallation_of_3matic_CancelNo_CancelYes(TestCategories.ACE);
        }

        #endregion

        #region Method
        private void CancelInstallation_of_3matic(string product, int cancelCount)
        {
            for (int i = 0; i < cancelCount; i++)
            {
                Init(product, 40, true, string.Format("-{0}", i));

                var count = 0;
                do
                {
                    //Thread.Sleep(10000);
                    var window = app.WaitForWindow((win) =>
                    {
                        return
                            (
                            IsNextAndCancelButtonExistsAndReady(win, count) ||
                            win.Exists<Button>(SearchCriteria.ByText("Complete")) ||
                            win.Exists<Button>(SearchCriteria.ByText("Install"))
                            );
                    });

                    if (window.Exists<Button>(SearchCriteria.ByText("Cancel")))
                    {
                        if (count == i)
                        {
                            window.GetButtonAndClick("Cancel");

                            app.WaitForWindow((win) =>
                            {
                                return win.Exists<Button>(SearchCriteria.ByText("Yes"));
                            }).GetButtonAndClick("Yes", string.Format("Confirmation-{0}", i));

                            app.WaitForWindow((win) =>
                            {
                                return win.Exists<Button>(SearchCriteria.ByText("Finish"));
                            }).GetButtonAndClick("Finish", string.Format("Interrupted-{0}", i));

                            app.WaitForWindow((win) =>
                            {
                                return win.Exists<Button>(SearchCriteria.ByText("OK"));
                            }).GetButtonAndClick("OK", string.Format("Failed-{0}", i));

                            break;
                        }
                        else
                        {
                            count++;
                        }
                    }

                    if (!window.GetButtonAndClick("Complete"))
                    {
                        if (!window.GetButtonAndClick("Install"))
                        {
                            if (window.Exists<Button>(SearchCriteria.ByText("Next")))
                            {
                                var button = window.Get<Button>(SearchCriteria.ByText("Next"));
                                if (!button.Enabled)
                                {   
                                    var checkBox = window.Get<CheckBox>();
                                    checkBox.Checked = true;
                                }
                                button.Click();
                            }
                        }
                    }
                } while (true);
            }
        }

        private void CancelInstallation_of_3matic_CancelNo_CancelYes(string product)
        {
            Init(product, 40);

            app.WaitForButtonAndClick(SearchCriteria.ByText("Cancel"));
            app.WaitForButtonAndClick(SearchCriteria.ByText("No"), "CancelYesNo");

            app.WaitForButtonAndClick(SearchCriteria.ByText("Cancel"));
            app.WaitForButtonAndClick(SearchCriteria.ByText("Yes"));

            app.WaitForWindow((win) =>
            {
                return win.Exists<Button>(SearchCriteria.ByText("Finish"));
            }).GetButtonAndClick("Finish", "InstallationInterruptedWindow");

            app.WaitForWindow((win) =>
            {
                return win.Exists<Button>(SearchCriteria.ByText("OK"));
            }).GetButtonAndClick("OK", "PackageInstallationFailed");
        }
        #endregion

        #region Helpers
        private bool IsNextAndCancelButtonExistsAndReady(Window window, int count)
        {
            if (count == 0)
            {
                if (window.Exists<Button>(SearchCriteria.ByText("Next")))
                {
                    var button = window.Get<Button>(SearchCriteria.ByText("Next"));
                    return button.Enabled;
                }
                return false;
            }
            else
                return window.Exists<Button>(SearchCriteria.ByText("Next")) || window.Exists<Button>(SearchCriteria.ByText("Cancel"));
        }
        #endregion
    }
}
