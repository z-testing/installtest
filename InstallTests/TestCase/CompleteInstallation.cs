using _3matic.TestHelpers;
using InstallTests.Extensions;
using InstallTests.UtilObjects;
using InstallTests.WindowObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;

namespace InstallTests.TestCase
{
	[TestClass]
	public class CompleteInstallation : TestBase
	{
		#region Part2 Complete Installation
		[TestCategory(TestCategories.Smoke)]
		[TestCategory(TestCategories.ACE)]
		[TestMethod]
		public void Setup_ACE()
		{
			ReinstallationUtil.TryToUninstall3maticIfInstalled(TestCategories.ACE);

			Init("ACE", 180, false);

			Windows.WelcomeWindow
				.GotoChooseSetupType()
				.SelectCompleteInstall()
				.StartInstall()
				.ClickFinish()
				.ClickOK();

			AddOrUpdateInterpreterRegistryKey(TestCategories.ACE);
		}

		[TestCategory(TestCategories.ACE)]
		[TestCategory(TestCategories.Regression)]
		[TestMethod]
		public void CompleteInstallation_of_3maticACE()
		{
			//arrange
			Arrange(TestCategories.ACE);

			//act & assert
			CompleteInstallation_of_3matic(TestCategories.ACE);
		}

		#endregion

		#region Method
		private void Arrange(string product)
		{
			ReinstallationUtil.TryToUninstall3maticIfInstalled(product);
		}

		private void CompleteInstallation_of_3matic(string product)
		{
			Init(product, 40);

			app.WaitForButtonAndClick(SearchCriteria.ByText("Next"), "WelcomeWindow");

			app.WaitForButtonAndClick(SearchCriteria.ByText("Browse..."));

			var defaultLocation = @"C:\Program Files\ACE\";
			var actualLocation = defaultLocation;
			var windows = app.GetWindows();
			if (windows.Count > 0)
			{
				var window = windows.Where(win => win.Enabled).First();
				var textbox = window.Get<TextBox>();
				actualLocation = textbox.Text;
				Assert.IsTrue(actualLocation.StartsWith(defaultLocation), string.Format("Default location is {0} instead of {1}x.x.", actualLocation, defaultLocation));
				window.GetButtonAndClick("Cancel");
			}

			app.WaitForButtonAndClick(SearchCriteria.ByText("Complete"), "SetupType");

			app.WaitForButtonAndClick(SearchCriteria.ByText("Back"));
			app.WaitForButtonAndClick(SearchCriteria.ByText("Complete"));

			app.WaitForButtonAndClick(SearchCriteria.ByText("Install"), "ReadyToInstall");
			app.TakeWindowsScreenshot("Installing");

			app.WaitForButtonAndClick(SearchCriteria.ByText("Finish"), "Completed");

			app.WaitForButtonAndClick(SearchCriteria.ByText("OK"), "SetupComplete");

			var filePath = string.Format("{0}\\3-matic.exe", actualLocation);
			Assert.IsTrue(File.Exists(filePath), string.Format("{0} is not created.", filePath));

			var path = Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory);
			actualLocation = actualLocation.TrimEnd('\\');
			var index = actualLocation.LastIndexOf('\\');
			var displayName = actualLocation.Substring(index + 1);
			var shortcutPath = string.Format("{0}\\{1}.lnk", path, displayName);
			Assert.IsTrue(File.Exists(shortcutPath), string.Format("Shortcut {0} is not created.", shortcutPath));

			CheckVersion(product);
		}

		private void CheckVersion(string product)
		{
			var productInfo = ConfigurationHelper.GetProductInformation(product);
			var productVersion = RegistryHelper.GetProductVersion(productInfo.Code);
			Assert.IsTrue(productInfo.Version.Equals(productVersion, StringComparison.InvariantCultureIgnoreCase), "Version number should be {0} instead of {1}", productInfo.Version, productVersion);
		}
		#endregion

		#region Helpers

		private void AddOrUpdateInterpreterRegistryKey(string product)
		{
			var productName = (product == TestCategories.ACE) ? string.Format("{0}_Research", product) : product;
            var keyName = ConfigurationHelper.GetInterpreterRegistryKey(productName);
            var valueName = "Interpreter path";
            var data = ConfigurationHelper.GetInterpreterPath();

            var processStartInfo = new ProcessStartInfo
            {
                FileName = "reg",
                Arguments = string.Format(@"add ""{0}"" /f /v ""{1}"" /t REG_SZ /d ""{2}""", keyName, valueName, data),
                //settings to get output/error
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            try
            {
                var process = Process.Start(processStartInfo);
                process.WaitForExit();
                if (process.ExitCode == 0) //successful
                {
                    var outputMessage = process.StandardOutput.ReadToEnd();
                    Trace.WriteLine(outputMessage);
                }
                else
                {
                    var errorMessage = process.StandardError.ReadToEnd();
                    Trace.WriteLine(errorMessage);
                } 
            }
            catch (Exception ex)
            {
                Trace.WriteLine(string.Format("Exception thrown in AddOrUpdateInterpreterRegistryKey for product: {0}", product));
                Trace.WriteLine(string.Format("Message: {0}", ex.Message));
                Trace.WriteLine(string.Format("Arguments: {0}", processStartInfo.Arguments));
            }
        }

        private void DeleteAppDataDirectories(string[] searchPatterns)
        {
            var path = string.Format(@"{0}\ACE\3-matic", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            if (Directory.Exists(path))
            {
                var directories = new List<string>();
                foreach(var pattern in searchPatterns)
                {
                    directories.AddRange(Directory.GetDirectories(path, pattern, SearchOption.TopDirectoryOnly));
                }                    
                    
                foreach (var directory in directories)
                {
                    Directory.Delete(directory, true);
                }
            }
        }

        #endregion
    }
}
