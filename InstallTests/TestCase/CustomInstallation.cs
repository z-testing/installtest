﻿using _3matic.TestHelpers;
using InstallTests.Extensions;
using InstallTests.UtilObjects;
using InstallTests.WindowObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Automation;
using TestStack.White.InputDevices;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.TreeItems;

namespace InstallTests.TestCase
{
    [TestClass]
    public class CustomInstallation : TestBase
    {
        [TestCleanup]
        public void Cleanup()
        {
            // During Cleanup not to uninstall other 3-matic programs
            // ReinstallationUtil.TryToUninstall3maticIfInstalled(TestCategories.ACE);
            // ReinstallationUtil.TryToUninstall3maticIfInstalled(TestCategories.BME);
            // ReinstallationUtil.TryToUninstall3maticIfInstalled(TestCategories.STL);
        }

        #region Part3 Custom Installation

        [TestCategory(TestCategories.ACE)]
        [TestCategory(TestCategories.Regression)]
        [TestMethod]
        [ScreenshotFolder("CustomLocation")]
        public void CustomInstallation_of_3maticACE_With_CustomDestinationFolder()
        {
            //arrange
            ReinstallationUtil.TryToUninstall3maticIfInstalled(TestCategories.ACE);

            //act & assert
            CustomInstallation_of_3matic_With_CustomDestinationFolder(TestCategories.ACE);

            ReinstallationUtil.TryToUninstall3maticIfInstalled(TestCategories.ACE);
        }

        [TestCategory(TestCategories.ACE)]
        [TestCategory(TestCategories.Regression)]
        [TestMethod]
        public void CustomInstallation_of_3maticACE_With_SelectedFeature()
        {
            CustomInstallation_of_3matic_With_AllFeatures(TestCategories.ACE, 1);
        }

        [TestCategory(TestCategories.ACE)]
        [TestCategory(TestCategories.Regression)]
        [TestMethod]
        [ScreenshotFolder("CustomInstallation_ParentFeatures")]
        public void CustomInstallation_of_3maticACE_With_SelectedParentFeature()
        {
            CustomInstallation_of_3matic_With_AllParentFeatures(TestCategories.ACE, 6);
        }

        #endregion

        #region Helpers

        protected override void Init(string productType, int waitPeriod, bool needScreenshot = true, string screenshot = "")
        {
            base.Init(productType, waitPeriod, needScreenshot, screenshot);

            app.WaitForButtonAndClick(SearchCriteria.ByText("Next"), "WelcomeWindow");
        }

        private void CustomInstallation_of_3matic_With_CustomDestinationFolder(string product)
        {
            var productInfo = ConfigurationHelper.GetProductInformation(product);
            var destinationFolder = string.Format(@"{0}\{1}", ConfigurationHelper.GetCustomInstallationDirectoryPath(), productInfo.Name);

            if (File.Exists(destinationFolder))
            {
                File.Delete(destinationFolder);
            }

            Init(product, 40);            

            app.WaitForButtonAndClick(SearchCriteria.ByText("Browse..."));

            var windows = app.GetWindows();
            if (windows.Count > 0)
            {
                var window = windows.Where(win => win.Enabled).First();
                var textbox = window.Get<TextBox>();
                textbox.Text = destinationFolder;
                window.GetButtonAndClick("OK", "Browse");
            }

            app.WaitForButtonAndClick(SearchCriteria.ByText("Complete"), "SetupType");
            InstallToCompletion();

            var filePath = string.Format("{0}\\3-matic.exe", destinationFolder);
            Assert.IsTrue(File.Exists(filePath), string.Format("{0} is not created.", filePath));
        }

        private void CustomInstallation_of_3matic_With_AllFeatures(string product, int totalFeatures)
        {
            for (var i = 0; i < totalFeatures; i++)
            {
                //arrange
                ReinstallationUtil.TryToUninstall3maticIfInstalled(product);

                //act & assert
                CustomInstallation_of_3matic(product, (tree) =>
                    {
                        var nodes = tree.Nodes;
                        var node = GetChild(nodes[0], 0);
                        SelectSingleFeature(node, i);

                        app.TakeWindowsScreenshot(string.Format("CustomSetup-{0}", i));
                    });

                Thread.Sleep(5000);
            }
        }

        private void CustomInstallation_of_3matic_With_AllParentFeatures(string product, int totalFeatures)
        {
            for (var i = 0; i < totalFeatures; i++)
            {
                //arrange
                ReinstallationUtil.TryToUninstall3maticIfInstalled(product);

                //act & assert
                CustomInstallation_of_3matic(product, (tree) =>
                    {
                        var nodes = tree.Nodes;
                        SelectSingleFeature(nodes[0], i);

                        app.TakeWindowsScreenshot(string.Format("CustomSetup-Parent{0}", i));
                    });

                Thread.Sleep(5000);
            }
        }

        private void CustomInstallation_of_3matic(string product, Action<Tree> selectFeaturesAction)
        {
            Init(product, 40);            

            app.WaitForButtonAndClick(SearchCriteria.ByText("Custom"), "SetupType");

            var windows = app.GetWindows();
            if (windows.Count > 0 && selectFeaturesAction != null)
            {
                var window = windows.First();
                var tree = window.Get<Tree>();
                selectFeaturesAction(tree);
            }

            app.WaitForButtonAndClick(SearchCriteria.ByText("Next"));
            InstallToCompletion();
        }

        private TreeNode GetChild(TreeNode treeNode, int childIndex)
        {
            if (treeNode.DisplayState == ExpandCollapseState.LeafNode)
                return null;

            if (treeNode.DisplayState != ExpandCollapseState.Expanded)
            {
                treeNode.Expand();
            }

            var nodes = treeNode.Nodes;
            if (nodes.Count > childIndex)
            {
                return nodes[childIndex];
            }

            return null;
        }

        private void SelectSingleFeature(TreeNode treeNode, int featureIndex)
        {
            var iconPoint = treeNode.Bounds.TopLeft;
            iconPoint.Offset(-5, 0);
            Mouse.Instance.Click(iconPoint); //feature menus are triggered by clicking on the icons
            var windows = app.GetWindows();
            if (windows.Count > 0)
            {
                var window = windows.First();
                var popup = window.Popup;
                var menu = popup.Items[featureIndex];
                var bounds = GetCenterPoint(menu.Bounds);
                Mouse.Instance.RightClick(bounds); //to select menu without clicking
                app.TakeWindowsScreenshot(string.Format("CustomFeatures-{0}", featureIndex)); //menu has no name
                menu.Click();
            }
        }

        private void InstallToCompletion()
        {
            app.WaitForButtonAndClick(SearchCriteria.ByText("Install"), "ReadyToInstall");
            app.WaitForButtonAndClick(SearchCriteria.ByText("Finish"), "Completed");
            app.WaitForButtonAndClick(SearchCriteria.ByText("OK"), "SetupComplete");
            Thread.Sleep(2500);
            var rebootWindow = app.GetWindows().FirstOrDefault();
            if (rebootWindow != null)
            {
                if (rebootWindow.Exists<Button>(SearchCriteria.ByText("OK")))
                {
                    rebootWindow.GetButtonAndClick("OK", "SetupReboot");
                }
            }
        }

        private Point GetCenterPoint(Rect rect)
        {
            return new Point(rect.Left + (rect.Width / 2), rect.Top + (rect.Height / 2));
        }

        #endregion
    }
}
