﻿using _3matic.TestHelpers;
using InstallTests.Extensions;
using InstallTests.UtilObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using TestStack.White;

namespace InstallTests.TestCase
{
    [TestClass]
    public class Installation : TestBase
    {
        private ProductInformation _productInformation;

        [TestCategory(TestCategories.ACE)]
        [TestCategory(TestCategories.Regression)]
        [TestMethod]
        public void CompleteInstallation_of_3maticACE_Will_Includes_Excludes_DLLs()
        {
            //arrange
            Arrange(TestCategories.ACE);
            var specificFile = "MatPISurgicaseOrtho.dll";
            var fileNamesToInclude =  new List<string> { specificFile };
            var fileNamesToExclude = DLLsToCheck();
            fileNamesToExclude.Remove(specificFile);

            //act & assert
            CheckDLLsAreIncluded(fileNamesToInclude);
            CheckDLLsAreNotIncluded(fileNamesToExclude);
        }

        #region Method

        private void Arrange(string product)
        {
            ReinstallationUtil.TryToUninstall3maticIfInstalled(product);
            _productInformation = ReinstallationUtil.TryToInstall3maticIfNotInstalled(product, false, true);
            TakeFolderScreenshot(_productInformation.ProgramPath);
        }

        private void CheckDLLsAreNotIncluded(IList<string> fileNamesToCheck)
        {
            var programPath = _productInformation.ProgramPath;
            var fileNames = Directory.GetFiles(programPath, "*.dll", SearchOption.AllDirectories).Select(path => GetLastSubstring(path).ToLower());
            foreach (var fileName in fileNamesToCheck)
            {
                Assert.IsFalse(fileNames.Contains(fileName.ToLower()), "{0} is included in {1}", fileName, programPath);
            }
        }

        private void CheckDLLsAreIncluded(IList<string> fileNamesToCheck)
        {
            var programPath = _productInformation.ProgramPath;
            var fileNames = Directory.GetFiles(programPath, "*.dll", SearchOption.AllDirectories).Select(path => GetLastSubstring(path).ToLower());
            foreach (var fileName in fileNamesToCheck)
            {
                Assert.IsTrue(fileNames.Contains(fileName.ToLower()), "{0} is not included in {1}", fileName, programPath);
            }
        }

        private IList<string> DLLsToCheck()
        {
            var fileNamesToCheck = new List<string>
            {
                "MatPIAFO.dll",
                "MatPIGraphs.dll", 
                "MatPIKievDev.dll", 
                "MatPILeuvenDev.dll", 
                "MatPIMasks.dll", 
                "MatPIMalaysia.dll", 
                "MatMDCKResearch.dll", 
                "MatPIMimicsRemesh.dll", 
                "MatPINastyaDev.dll", 
                "MatPILVF.dll", 
                "MatPICGB.dll", 
                "MatPIPCA.dll", 
                "MatPIResearch.dll", 
                "MatPISmoothCurves.dll", 
                "MatPISurgicaseOrtho.dll", 
                "MatPISurgicaseCMD.dll", 
                "MatPITextures.dll"
            };
            return fileNamesToCheck;
        }

        #endregion

        #region Helpers

        private string GetLastSubstring(string fullString)
        {
            var index = fullString.LastIndexOf('\\');
            var substring = fullString.Substring(index + 1);
            return substring;
        }

        private void TakeFolderScreenshot(string directory)
        {
            if (Directory.Exists(directory))
            {
                var processName = "Explorer";
                var processes = Process.GetProcessesByName(processName);
                foreach (var process in processes)
                {
                    process.Kill();
                }

                Process.Start(processName, directory);
                Thread.Sleep(2000);

                processes = Process.GetProcessesByName(processName);
                foreach (var process in processes)
                {
                    try
                    {
                        var application = Application.Attach(process);
                        var window = application.GetWindows().FirstOrDefault();
                        if (window != null)
                        {
                            window.TakeScreenshot("ProgramFolder");
                            window.Close();
                            break;
                        }
                    }
                    catch (WhiteException ex)
                    {
                        Trace.WriteLine(string.Format("Exception during folder screenshot: {0}", ex.Message));
                    }
                }               
            }
        }

        #endregion
    }
}
