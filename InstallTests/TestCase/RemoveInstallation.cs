using _3matic.TestHelpers;
using InstallTests.Extensions;
using InstallTests.UtilObjects;
using InstallTests.WindowObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.IO;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;

namespace InstallTests.TestCase
{
    [TestClass]
    public class RemoveInstallation : TestBase
    {
        private ProductInformation _productInformation;

        [TestCategory(TestCategories.ACE)]
        [TestCategory(TestCategories.Regression)]
        [TestMethod]
        public void RemoveInstallation_of_3maticACE_Will_Delete_3maticACE_Folder()
        {
            //arrange
            Arrange(TestCategories.ACE);

            //act
            RemoveInstallation_of_3matic();

            //assert
            RemoveInstallationAssert();
        }

        #region Methods

        private void Arrange(string product)
        {
            ReinstallationUtil.TryToUninstall3maticIfInstalled(product);
            _productInformation = ReinstallationUtil.TryToInstall3maticIfNotInstalled(product);
        }

        private void RemoveInstallation_of_3matic()
        {
            var displayName = _productInformation.Name;
            var productCode = RegistryHelper.GetProductCode(displayName);
            if (string.IsNullOrEmpty(productCode))
            {
                Assert.Fail("Unable to locate {0} product code. Please make sure it is installed.", displayName);
            }

            var processStartInfo = new ProcessStartInfo
            {
                FileName = "MsiExec.exe",
                Arguments = string.Format("/i{0}", productCode)
            };

            var application = Application.Launch(processStartInfo);

            application.WaitForButtonAndClick(SearchCriteria.ByText("Next"), "3-maticWindow");

            var removeWindow = new ChangeInstallationWindow(application);
            removeWindow.ConfirmRemove();

            var window = application.WaitForWindow((win) =>
            {
                return win.Exists<Button>(SearchCriteria.ByText("Finish")) || win.Exists<Button>(SearchCriteria.ByText("OK"));
            });
            if (window.Exists<Button>(SearchCriteria.ByText("OK")))
            {
                window.GetButtonAndClick("OK");
            }

            application.WaitForButtonAndClick(SearchCriteria.ByText("Finish"));

            var completedWindow = new RestartSystemWindow(application);
            completedWindow.ClickNo();
        }

        private void RemoveInstallationAssert()
        {
            var deleted = !Directory.Exists(_productInformation.ProgramPath);
            Assert.IsTrue(deleted, "{0} folder or files under it are not deleted after remove installation.", _productInformation.ProgramPath);
            //Note: preferences_zimmer.xml.saved file not deleted
        }

        #endregion
    }
}
