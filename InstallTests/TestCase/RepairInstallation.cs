﻿using _3matic.TestHelpers;
using InstallTests.Extensions;
using InstallTests.UtilObjects;
using InstallTests.WindowObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.IO;
using TestStack.White;
using TestStack.White.UIItems.Finders;

namespace InstallTests.TestCase
{
    [TestClass]
    public class RepairInstallation : TestBase
    {
        private ProductInformation _productInformation;

        [TestCategory(TestCategories.ACE)]
        [TestCategory(TestCategories.Regression)]
        [TestMethod]
        public void RepairInstallation_of_3maticACE_Will_Fix_Missing_File_And_Shortcut()
        {
            //arrange
            Arrange(TestCategories.ACE);

            //act
            RepairInstallation_of_3matic();

            //assert
            RepairInstallationAssert();
        }

        [TestCategory(TestCategories.ACE)]
        [TestCategory(TestCategories.Regression)]
        [TestMethod]
        [ScreenshotFolder("RepairInstallationEXE")]
        public void RepairInstallation_of_3maticACE_Using_EXE()
        {
            //arrange
            ArrangeForEXE(TestCategories.ACE);

            //act
            RepairInstallation_of_3matic();

            //assert
            RepairInstallationAssert();
        }

        #region Methods

        private void Arrange(string product)
        {
            ReinstallationUtil.TryToUninstall3maticIfInstalled(product);
            _productInformation = ReinstallationUtil.TryToInstall3maticIfNotInstalled(product);
        
            var programPath = GetExecutableFilePath();
            DeleteFile(programPath);

            var shortcut = GetShortcutPath();
            DeleteFile(shortcut);
        }

        private void ArrangeForEXE(string product)
        {
            ReinstallationUtil.TryToUninstall3maticIfInstalled(product);
            _productInformation = ReinstallationUtil.TryToInstall3maticIfNotInstalled(product, true);

            var programPath = GetExecutableFilePath();
            DeleteFile(programPath);

            var shortcut = GetShortcutPath();
            DeleteFile(shortcut);
        }

        private void RepairInstallation_of_3matic()
        {
            var displayName = _productInformation.Name;
            var productCode = RegistryHelper.GetProductCode(displayName);
            if (string.IsNullOrEmpty(productCode))
            {
                Assert.Fail("Unable to locate {0} product code. Please make sure it is installed.", displayName);
            }

            var processStartInfo = new ProcessStartInfo
            {
                FileName = "MsiExec.exe",
                Arguments = string.Format("/i{0}", productCode)
            };

            var application = Application.Launch(processStartInfo);

            application.WaitForButtonAndClick(SearchCriteria.ByText("Next"), "3-maticWindow");

            var repairWindow = new ChangeInstallationWindow(application);
            repairWindow.ConfirmRepair();

            application.WaitForButtonAndClick(SearchCriteria.ByText("Finish"));
        }

        private void RepairInstallationAssert()
        {
            var missingFile = GetExecutableFilePath();
            Assert.IsTrue(File.Exists(missingFile), string.Format("{0} is not repaired.", missingFile));

            var missingShortcut = GetShortcutPath();
            Assert.IsTrue(File.Exists(missingShortcut), string.Format("{0} is not repaired.", missingShortcut));
        }

        #endregion

        #region Helpers

        private string GetExecutableFilePath()
        {
            var filePath = string.Format("{0}\\3-matic.exe", _productInformation.ProgramPath);
            return filePath;
        }

        private string GetShortcutPath()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory);
            var displayName = _productInformation.Name;
            var shortcutPath = string.Format("{0}\\{1}.lnk", path, displayName);
            return shortcutPath;
        }

        private void DeleteFile(string fileToDelete)
        {
            Assert.IsTrue(File.Exists(fileToDelete), string.Format("{0} does not exist.", fileToDelete));

            File.Delete(fileToDelete);

            Assert.IsFalse(File.Exists(fileToDelete), string.Format("{0} is not deleted.", fileToDelete));
        }

        #endregion
    }
}
