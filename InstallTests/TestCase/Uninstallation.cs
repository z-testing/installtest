﻿using _3matic.TestHelpers;
using InstallTests.Extensions;
using InstallTests.UtilObjects;
using InstallTests.WindowObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.IO;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;

namespace InstallTests.TestCase
{
    [TestClass]
    public class Uninstallation : TestBase
    {
        private ProductInformation _productInformation;
        
        #region ACE

        //NOTE: this will cause system restart
        [TestCategory(TestCategories.ACE)]
        [TestMethod]
        [Ignore]
        public void Uninstallation_of_3maticACE_With_Clicking_Yes_Will_Restart_System()
        {
            //arrange
            Arrange(TestCategories.ACE);

            //act & assert
            Uninstallation_of_3matic_With_Clicking_Yes_Will_Restart_System();
        }

        [TestCategory(TestCategories.ACE)]
        [TestCategory(TestCategories.Regression)]
        [TestMethod]
        public void Uninstallation_of_3maticACE_With_Clicking_No_Will_Remove_Shortcut_Icon()
        {
            //arrange
            Arrange(TestCategories.ACE);

            //act & assert
            Uninstallation_of_3matic_With_Clicking_No_Will_Remove_Shortcut_Icon();
        }

        [TestCategory(TestCategories.ACE)]
        [TestCategory(TestCategories.Regression)]
        [TestMethod]
        [ScreenshotFolder("UninstallationWithWarning")]
        public void Uninstallation_of_3maticACE_Will_Be_Interupted_When_There_Is_A_3maticACE_Opened()
        {
            //arrange
            Arrange(TestCategories.ACE);

            //act & assert
            Uninstallation_of_3matic_Will_Be_Interupted_When_There_Is_A_3matic_Opened();
        }

        #endregion

        #region Methods

        private void Arrange(string product)
        {
            ReinstallationUtil.TryToUninstall3maticIfInstalled(product);
            _productInformation = ReinstallationUtil.TryToInstall3maticIfNotInstalled(product);
        }

        private void Uninstallation_of_3matic_With_Clicking_Yes_Will_Restart_System()
        {
            var application = Uninstallation_Normal_Flow();

            var completedWindow = new RestartSystemWindow(application);
            completedWindow.ClickYes();

            //how do I verify this?
        }

        private void Uninstallation_of_3matic_With_Clicking_No_Will_Remove_Shortcut_Icon()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory);
            var displayName = _productInformation.Name;            
            var shortcut = string.Format("{0}\\{1}.lnk", path, displayName);
            Assert.IsTrue(File.Exists(shortcut), "Shortcut does not exist.");

            var application = Uninstallation_Normal_Flow();
            
            var completedWindow = new RestartSystemWindow(application);
            completedWindow.ClickNo();

            Assert.IsFalse(File.Exists(shortcut), "Shortcut is not removed after uninstallation.");
        }

        private void Uninstallation_of_3matic_Will_Be_Interupted_When_There_Is_A_3matic_Opened()
        {
            var process = Process.Start(string.Format("{0}\\3-matic.exe", _productInformation.ProgramPath));

            var application = Uninstallation_Normal_Flow(false);

            var window = application.WaitForWindow((win) => 
                   {
                       return win.Exists<Button>(SearchCriteria.ByText("OK")) && win.Exists<Button>(SearchCriteria.ByText("Cancel"));
                   });

            Assert.IsNotNull(window, "No warning shown when there is a 3-matic application opened.");

            application.TakeWindowsScreenshot("UninstallWarning");
            
            var interuptionWindow = new CloseApplicationsWarningWindow(window);
            interuptionWindow.ClickCancel();

            process.Kill();
        }

        #endregion

        #region Helpers

        private Application Uninstallation_Normal_Flow(bool untilCompletion = true)
        {
            var displayName = _productInformation.Name;            
            var productCode = RegistryHelper.GetProductCode(displayName);
            if (string.IsNullOrEmpty(productCode))
            {
                Assert.Fail("Unable to locate {0} product code. Please make sure it is installed.", displayName);
            }

            var processStartInfo = new ProcessStartInfo
            {
                FileName = "MsiExec.exe",
                Arguments = string.Format("/uninstall {0}", productCode)
            };

            var application = Application.Launch(processStartInfo);
            var window = new WindowsInstallerWindow(application);
            window.ClickYes();

            application.TakeWindowsScreenshot("ProgressWindow");
            
            if (untilCompletion)
            {
               var restartWindow = application.WaitForWindow((win) => 
                    {
                        return (win.Exists<Button>(SearchCriteria.ByText("Yes")) && win.Exists<Button>(SearchCriteria.ByText("No"))) 
                            || win.Exists<Button>(SearchCriteria.ByText("OK"));
                    });
               if (restartWindow.Exists<Button>(SearchCriteria.ByText("OK")))
               {
                   restartWindow.GetButtonAndClick("OK");
                   restartWindow = application.WaitForWindow((win) =>
                       {
                           return win.Exists<Button>(SearchCriteria.ByText("Yes")) && win.Exists<Button>(SearchCriteria.ByText("No"));
                       });
               }

               restartWindow.TakeScreenshot("PromptRestart");
            }

            return application;
        }
        
        #endregion
    }
}
