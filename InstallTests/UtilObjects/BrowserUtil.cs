﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Management;
using System.Threading;
using System.Windows.Automation;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Custom;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WindowStripControls;

namespace InstallTests.UtilObjects
{
    //TODO: make implementation thread-safe

    public class BrowserUtil
    {
        private static BrowserUtil _instance;
        private ManagementEventWatcher _eventWatcher;
        private Func<string> _findAddressOnBrowser;

        private const string _ieProcessName = "iexplore";
        private const string _chromeProcessName = "chrome";
        private const string _firefoxProcessName = "firefox";
        private const string _edgeProcessName = "applicationframehost";

        private BrowserUtil()
        {
            _eventWatcher = new ManagementEventWatcher(new WqlEventQuery("SELECT * FROM Win32_ProcessStartTrace"));
            _eventWatcher.EventArrived += new EventArrivedEventHandler(EventArrived);
            _eventWatcher.Start();

            try
            {
                Process.Start("https://www.google.com/");
            }
            catch (Win32Exception)
            {
                Process.Start("https://www.google.com/");
            }
        }

        public static void Setup()
        {
            if (_instance == null)
            {
                _instance = new BrowserUtil();
            }
        }

        public static BrowserUtil Instance
        {
            get
            {
                Setup();
                return _instance;
            }
        }

        public bool IsReady()
        {
            return _eventWatcher == null;
        }

        public string GetURL()
        {
            if (_findAddressOnBrowser != null)
            {
                Thread.Sleep(ConfigurationHelper.GetBrowserURLWaitTime());            
                return _findAddressOnBrowser();
            }
            return string.Empty;
        }

        public void MinimizeOrCloseWindow()
        {
            var processes = new List<string>
            {
                _ieProcessName,
                _chromeProcessName,
                _firefoxProcessName,
                _edgeProcessName
            };

            foreach (var process in processes)
            {
                var browserWindow = GetBrowserWindow(process);
                if (browserWindow != null)
                {
                    browserWindow.DisplayState = DisplayState.Minimized;
                    if (browserWindow.DisplayState != DisplayState.Minimized)
                    {
                        browserWindow.Close();
                        var browserProcesses = Process.GetProcessesByName(process);
                        foreach (var browserProcess in browserProcesses)
                        {
                            if (!browserProcess.HasExited)
                            {
                                browserProcess.Kill();
                            }
                        }
                    }
                }
            }
        }

        #region Helpers

        private void EventArrived(object sender, EventArrivedEventArgs e)
        {
            var processName = e.NewEvent.Properties["ProcessName"].Value;
            if (processName != null && !string.IsNullOrEmpty(processName.ToString()))
            {
                var processNameStr = processName.ToString().ToLower().Replace(".exe", "");
                switch (processNameStr)
                {
                    case _ieProcessName:
                    case _chromeProcessName:
                    case _firefoxProcessName:
                    case "microsoftedge":
                        PrepareBrowser(sender as ManagementEventWatcher, processNameStr);
                        break;
                }
            }
        }

        private void PrepareBrowser(ManagementEventWatcher sender, string processName)
        {
            sender.EventArrived -= new EventArrivedEventHandler(EventArrived);

            var processes = Process.GetProcessesByName(processName);
            foreach (Process process in processes)
            {
                if (!process.HasExited)
                {
                    process.Kill();
                }
            }

            switch (processName)
            {
                case _ieProcessName:
                    _findAddressOnBrowser = GetAddressInIE;
                    break;
                case _chromeProcessName:
                    _findAddressOnBrowser = GetAddressInChrome;
                    break;
                case _firefoxProcessName:
                    _findAddressOnBrowser = GetAddressInFirefox;
                    break;
                case "microsoftedge":
                    _findAddressOnBrowser = GetAddressInEdge;
                    break;
            }

            _eventWatcher = null;
            sender.Stop();
        }

        private string GetAddressInIE()
        {
            var browserWindow = GetBrowserWindow(_ieProcessName);
            if (browserWindow != null)
            {
                foreach (var item in browserWindow.Items)
                {
                    if (item.GetType() == typeof(TextBox))
                    {
                        var element = item.AutomationElement;
                        var edit = element.FindFirst(TreeScope.Subtree, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Edit));
                        if (edit != null)
                        {
                            var value = ((ValuePattern)edit.GetCurrentPattern(ValuePattern.Pattern)).Current.Value as string;
                            return value;
                        }
                    }
                }
            }

            return string.Empty;
        }

        private string GetAddressInChrome()
        {
            var browserWindow = GetBrowserWindow(_chromeProcessName);
            if (browserWindow != null)
            {
                foreach (var item in browserWindow.Items)
                {
                    if (item.GetType() == typeof(ToolStrip))
                    {
                        var toolstrip = item as ToolStrip;
                        var items = toolstrip.Items;
                        foreach (var i in items)
                        {
                            if (i.GetType() == typeof(CustomUIItem))
                            {
                                var element = i.AutomationElement;
                                var edit = element.FindFirst(TreeScope.Subtree, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Edit));
                                if (edit != null)
                                {
                                    var value = ((ValuePattern)edit.GetCurrentPattern(ValuePattern.Pattern)).Current.Value as string;
                                    return value;
                                }
                            }
                        }
                    }
                }
            }

            return string.Empty;
        }

        private string GetAddressInFirefox()
        {
            var browserWindow = GetBrowserWindow(_firefoxProcessName);
            if (browserWindow != null)
            {
                foreach (var item in browserWindow.Items)
                {
                    if (item.GetType() == typeof(ToolStrip))
                    {
                        var toolstrip = item as ToolStrip;
                        var element = toolstrip.AutomationElement;
                        var edit = element.FindFirst(TreeScope.Subtree, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Edit));
                        if (edit != null)
                        {
                            var value = ((ValuePattern)edit.GetCurrentPattern(ValuePattern.Pattern)).Current.Value as string;
                            return value;
                        }
                    }
                }
            }

            return string.Empty;
        }

        private string GetAddressInEdge()
        {
            var browserWindow = GetBrowserWindow(_edgeProcessName);
            if (browserWindow != null)
            {
                var element = browserWindow.AutomationElement;

                var window = element.FindFirst(TreeScope.Children, new AndCondition(
                        new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Window),
                        new PropertyCondition(AutomationElement.NameProperty, "Microsoft Edge")));

                if (window != null)
                {
                    var edit = window.FindFirst(TreeScope.Children, new PropertyCondition(AutomationElement.AutomationIdProperty, "addressEditBox"));
                    if (edit != null)
                    {
                        var value = ((TextPattern)edit.GetCurrentPattern(TextPattern.Pattern)).DocumentRange.GetText(int.MaxValue);
                        return value;
                    }
                }
            }

            return string.Empty;
        }

        private Window GetBrowserWindow(string processName)
        {
            Window browserWindow = null;
            var processId = -1;
            Process[] processes = Process.GetProcessesByName(processName);
            foreach (Process process in processes)
            {
                if (process.MainWindowHandle != IntPtr.Zero && !string.IsNullOrEmpty(process.MainWindowTitle))
                {
                    processId = process.Id;
                    break;
                }
            }

            if (processId != -1)
            {
                var application = Application.Attach(processId);
                var windows = application.GetWindows();
                if (windows.Count > 0)
                {
                    browserWindow = windows[0];
                }
            }
            return browserWindow;
        }

        #endregion
    }
}
