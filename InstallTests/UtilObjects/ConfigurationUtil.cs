using _3matic.TestHelpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using WindowsInstaller;

namespace InstallTests.UtilObjects
{
    public static class ConfigurationHelper
    {
        public static string GetScreenshotsDirectoryPath()
        {
            return ConfigurationManager.AppSettings["Screenshots_DirectoryPath"];
        }

        public static string GetCustomInstallationDirectoryPath()
        {
            return ConfigurationManager.AppSettings["CustomInstallation_DirectoryPath"];
        }

        public static int GetWaitForWindowTimeout()
        {
            var timeoutStr = ConfigurationManager.AppSettings["WaitForWindow_Timeout"];
            return ParseTime(timeoutStr);
        }

        public static int GetBrowserURLWaitTime()
        {
            var timeStr = ConfigurationManager.AppSettings["BrowserURL_WaitTime"];
            return ParseTime(timeStr);
        }

        public static bool GetScreenshotsEnabled()
        {
            var enabledStr = ConfigurationManager.AppSettings["Screenshots_Enabled"];
            bool enabled = false;
            Boolean.TryParse(enabledStr, out enabled);
            return enabled;
        }

        public static ProductInformation GetProductInformation(string product)
        {
            ProductInformation productInfo = null;
            if (settings.ContainsKey(product))
            {
                productInfo = settings[product];
            }
            else
            {
                var msiInstallationPath = GetProductInstallationPath(product, InstallationType.MSI);
                var installerType = Type.GetTypeFromProgID("WindowsInstaller.Installer");
                var installer = (Installer)Activator.CreateInstance(installerType);
                var database = installer.OpenDatabase(msiInstallationPath, MsiOpenDatabaseMode.msiOpenDatabaseModeReadOnly);
                var view = database.OpenView("SELECT * FROM Property");
                view.Execute(null);

                var record = view.Fetch();
                string name = string.Empty;
                string version = string.Empty;
                string code = string.Empty;
                while (record != null)
                {
                    var key = record.get_StringData(1);
                    var value = record.get_StringData(2);
                    switch (key)
                    {
                        case "ProductName":
                            name = value;
                            break;
                        case "ProductVersion":
                            version = value;
                            break;
                        case "ProductCode":
                            code = value;
                            break;
                    }
                    record = view.Fetch();
                }

                var exeInstallationPath = GetProductInstallationPath(product, InstallationType.EXE);
                productInfo = new ProductInformation(product, name, version, code, msiInstallationPath, exeInstallationPath);                
                settings.Add(product, productInfo);
            }
            return productInfo;
        }

        public static string GetInterpreterRegistryKey(string product)
        {
           return ConfigurationManager.AppSettings[string.Format("{0}_InterpreterRegistryKey", product)];
        }

        public static string GetInterpreterPath()
        {
            return ConfigurationManager.AppSettings["Interpreter_Path"];
        }

        public static string GetProductInstallationPath(string product)
        {
            return GetProductInstallationPath(product, InstallationType.EXE);
        }

        #region Helpers

        private static IDictionary<string, ProductInformation> settings = new Dictionary<string, ProductInformation>();
        
        private static string GetProductInstallationPath(string product, InstallationType installationType)
        {
            var applicationRootPath = string.Empty;
            switch (product)
            {
                case TestCategories.ACE:
                    applicationRootPath = ConfigurationManager.AppSettings["ACE_InstallationRootPath"];
                    break;
			}

            var pattern = string.Empty;
            switch (installationType)
            {
                case InstallationType.EXE:
                    pattern = "*setup*.exe";
                    break;
                case InstallationType.MSI:
                    pattern = "*.msi";
                    break;
            }

            var installationPath = GetLatestFileLocation(applicationRootPath, pattern, product);
            installationPath = GetProductInstallationLocalPath(installationPath, product, pattern);
            return installationPath;
        }

        private static string GetLatestFileLocation(string directory, string pattern, string product)
        {
            if (directory.Trim().Length == 0)
                return string.Empty;

            var path = GetLatestModifiedDirectory(directory, product);

            var file = WalkDirectoryTree(path, pattern);

            if (file == null)
                throw new Exception(string.Format("File not found in: {0}", path.FullName));

            return file.Directory + "\\" + file.ToString();
        }

        private static DirectoryInfo GetLatestModifiedDirectory(string path, string product = "")
        {
            DirectoryInfo latestDirectory;

            if (product == "BME")
            {
                latestDirectory = new DirectoryInfo(path).GetDirectories()
                  .Where(d => d.Name.Contains("3-matic Research"))
                     .OrderByDescending(d => d.LastWriteTimeUtc).First();
            }
            else
            {
                latestDirectory = new DirectoryInfo(path).GetDirectories()
                      .OrderByDescending(d => d.LastWriteTimeUtc).First();
            }

            return latestDirectory;
        }

        private static FileInfo WalkDirectoryTree(DirectoryInfo root, string pattern)
        {
            System.IO.FileInfo files = null;
            System.IO.DirectoryInfo[] subDirs = null;

            // First, process all the files directly under this folder
            try
            {
                files = (from f in root.GetFiles(pattern)
                         orderby f.LastWriteTime descending
                         select f).First();
            }
            catch (Exception e)
            { }

            if (files == null)
            {
                // Now find all the subdirectories under this directory.
                subDirs = root.GetDirectories();

                foreach (System.IO.DirectoryInfo dirInfo in subDirs)
                {
                    // Resursive call for each subdirectory.
                    files = WalkDirectoryTree(dirInfo, pattern);

                    if (files != null)
                    {
                        return files;
                    }
                }
            }

            return files;
        }

        private static string GetProductInstallationLocalPath(string installationPath, string product, string pattern)
        {
            if (GetLocalInstallationRootPathEnabled())
            {
                var fileName = installationPath.Substring(installationPath.LastIndexOf('\\') + 1);
                var localPath = string.Format(@"C:\installation\{0}", product);
                var localInstaller = string.Format("{0}\\{1}", localPath, fileName);
                if (Directory.Exists(localPath))
                {
                    var installers = Directory.GetFiles(localPath, pattern, SearchOption.TopDirectoryOnly);
                    var localFileNames = installers.Select(i => i.Substring(i.LastIndexOf('\\') + 1));
                    if (!localFileNames.Contains(fileName))
                    {
                        File.Copy(installationPath, localInstaller);
                    }

                    foreach (var name in installers)
                    {
                        if (name.Contains(fileName))
                            continue;
                        File.Delete(name);
                    }
                }
                else
                {
                    Directory.CreateDirectory(localPath);
                    File.Copy(installationPath, localInstaller);
                }
                return localInstaller;
            }

            return installationPath;
        }

        private static int ParseTime(string timeStr)
        {
            int time = -1;
            if (!Int32.TryParse(timeStr, out time) || time < 0)
            {
                time = 0;
            }
            return time;
        }

        private static bool GetLocalInstallationRootPathEnabled()
        {
            var enabledStr = ConfigurationManager.AppSettings["LocalInstallationRootPath_Enabled"];
            bool enabled = false;
            Boolean.TryParse(enabledStr, out enabled);
            return enabled;
        }

        #endregion

        internal enum InstallationType
        {
            EXE,
            MSI
        }
    }
}
