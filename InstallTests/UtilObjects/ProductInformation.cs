﻿using IWshRuntimeLibrary;
using System;

namespace InstallTests.UtilObjects
{
    public class ProductInformation
    {
        public string Category { get; private set; }

        public string Name { get; private set; }

        public string Version { get; private set; }

        public string Code { get; private set; }

        public string InstallationPathMSI { get; private set; }

        public string InstallationPathEXE { get; private set; }

        public string ProgramPath { get; private set; }

        public ProductInformation(string category, string name, string version, string code, string installationPathMSI, string installationPathEXE)
        {
            Category = category;
            Name = name;
            Version = version;
            Code = code;
            InstallationPathMSI = installationPathMSI;
            InstallationPathEXE = installationPathEXE;
        }

        public void UpdateProgramPath()
        {
            if (string.IsNullOrEmpty(ProgramPath))
            {
                var path = Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory);
                var shortcutPath = string.Format("{0}\\{1}.lnk", path, Name);
                if (System.IO.File.Exists(shortcutPath))
                {
                    var shell = new WshShell();
                    var lnk = shell.CreateShortcut(shortcutPath) as IWshShortcut;
                    if (lnk != null)
                    {
                        ProgramPath = lnk.WorkingDirectory;
                    }
                }
            }
        }
    }
}
