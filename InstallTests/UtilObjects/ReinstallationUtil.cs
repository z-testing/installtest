﻿using _3matic.TestHelpers;
using InstallTests.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;

namespace InstallTests.UtilObjects
{
    public static class ReinstallationUtil
    {
        public static ProductInformation TryToInstall3maticIfNotInstalled(string product, bool useEXE = false, bool needScreenshot = false)
        {
            IList<string> reinstallSteps = null;
            switch (product)
            {
                case TestCategories.ACE:
                    reinstallSteps = ReinstallACESteps();
                    break;
            }

            if (reinstallSteps != null)
            {
                if (useEXE)
                {
                    reinstallSteps.Add("OK"); //Note: OK button exists if execute via *.exe
                }
                return TryToInstallIfNotInstalled(product, reinstallSteps, useEXE, needScreenshot);
            }

            return null;
        }

        public static void TryToUninstall3maticIfInstalled(string product)
        {
            var productInfo = ConfigurationHelper.GetProductInformation(product);
            var productCode = RegistryHelper.GetProductCode(productInfo.Name);
            if (!string.IsNullOrEmpty(productCode))
            {
                Uninstallation(productCode);
            }
        }

        #region Private Methods

        private static ProductInformation TryToInstallIfNotInstalled(string product, IList<string> reinstallSteps, bool useEXE, bool needScreenshot)
        {
            var productInfo = ConfigurationHelper.GetProductInformation(product);
            var productCode = RegistryHelper.GetProductCode(productInfo.Name);
            if (productCode != productInfo.Code)
            {
                var installationPath = useEXE ? productInfo.InstallationPathEXE : productInfo.InstallationPathMSI;
                Reinstallation(installationPath, reinstallSteps, needScreenshot);
            }
            productInfo.UpdateProgramPath();
            return productInfo;
        }

        private static IList<string> ReinstallACESteps()
        {
            var buttonNames = new List<string>
            {
                "Next",
                "Complete",
                "Install",
                "Finish",
                //"OK" //Note: no OK button if execute via *.msi
            };
            return buttonNames;
        }

        private static IList<string> ReinstallBMESteps()
        {
            var buttonNames = new List<string>
            {
                "Next",
                "Next",
                "Complete",
                "Install",
                "Finish",
                //"OK" //Note: no OK button if execute via *.msi
            };
            return buttonNames;
        }

        private static IList<string> ReinstallSTLSteps() //similar with BME
        {
            var buttonNames = new List<string>
            {
                "Next",
                "Next",
                "Complete",
                "Install",
                "Finish",
                //"OK" //Note: no OK button if execute via *.msi
            };
            return buttonNames;
        }

        private static void Reinstallation(string installationPath, IList<string> buttonNames, bool needScreenshot)
        {
            var buttonsToClick = new Queue<SearchCriteria>(buttonNames.Select(name => SearchCriteria.ByText(name)));

            var application = RunInstaller(installationPath);

            var screenshotCount = 1;

            while (buttonsToClick.Count > 0)
            {
                var criteria = buttonsToClick.Dequeue();
                var window = application.WaitForWindow((win) =>
                      {
                          var found = false;
                          found = win.Exists<Button>(criteria);
                          if (found)
                          {
                              var item = win.Get<Button>(criteria);
                              found = item.Enabled || win.Exists<CheckBox>();
                          }
                          return found;
                      });

                var button = window.Get<Button>(criteria);
                if (!button.Enabled)
                {
                    var checkBox = window.Get<CheckBox>();
                    checkBox.Checked = true;
                }
                if (needScreenshot)
                {
                    window.TakeScreenshot(string.Format("InstallationWindow{0}-Click{1}", screenshotCount++, button.Text));
                }
                button.Click();
            }
        }

        private static void Uninstallation(string productCode)
        {
            var processStartInfo = new ProcessStartInfo
            {
                FileName = "MsiExec.exe",
                Arguments = string.Format("/X{0}", productCode)
            };

            var application = Application.Launch(processStartInfo);

            application.WaitForButtonAndClick(SearchCriteria.ByText("Yes"));
            var window = application.WaitForWindow((win) =>
            {
                return win.Exists<Button>(SearchCriteria.ByText("No")) || win.Exists<Button>(SearchCriteria.ByText("OK"));
            });
            if (window.Exists<Button>(SearchCriteria.ByText("OK")))
            {
                window.GetButtonAndClick("OK");
            }

            application.WaitForButtonAndClick(SearchCriteria.ByText("No"));
        }

        private static Application RunInstaller(string installationPath)
        {
            var processStartInfo = new ProcessStartInfo();

            if (installationPath.EndsWith("msi", StringComparison.InvariantCultureIgnoreCase))
            {
                processStartInfo.FileName = "MsiExec.exe";
                processStartInfo.Arguments = string.Format("/i \"{0}\"", installationPath);
            }
            else if (installationPath.EndsWith("exe", StringComparison.InvariantCultureIgnoreCase))
            {
                processStartInfo.FileName = installationPath;
                processStartInfo.CreateNoWindow = true;
                processStartInfo.UseShellExecute = false;
                processStartInfo.Verb = "runas";
            }

            var application = Application.Launch(processStartInfo);
            return application;
        }

        #endregion
    }
}
