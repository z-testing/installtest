﻿using System;

namespace InstallTests.UtilObjects
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class ScreenshotFolderAttribute : Attribute
    {
        public string FolderName { get; private set; }

        public ScreenshotFolderAttribute(string folderName)
        {
            FolderName = folderName;
        }
    }
}
