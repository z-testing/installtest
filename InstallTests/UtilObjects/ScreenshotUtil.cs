﻿using _3matic.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using TestStack.White;
using Size = System.Drawing.Size;

namespace InstallTests.UtilObjects
{
    public class ScreenshotUtil
    {
        //TODO: housekeeping of generated files

        private static ScreenshotUtil _instance;
        private readonly string _runPath;

        private ScreenshotUtil()
        {
            var isEnabled = ConfigurationHelper.GetScreenshotsEnabled();
            if (isEnabled)
            {
                var rootDirectory = ConfigurationHelper.GetScreenshotsDirectoryPath();
                if (!Directory.Exists(rootDirectory))
                {
                    Directory.CreateDirectory(rootDirectory);
                }

                var dateStr = DateTime.Now.ToString("dd-MM-yyyy");
                rootDirectory = string.Format(@"{0}\{1}", rootDirectory, dateStr);
                if (!Directory.Exists(rootDirectory))
                {
                    Directory.CreateDirectory(rootDirectory);
                }

                var runStrFormat = @"{0}\Run{1}";
                for (var i = 1; i <= 1000; i++)
                {
                    var runDirectory = string.Format(runStrFormat, rootDirectory, i);
                    if (!Directory.Exists(runDirectory))
                    {
                        Directory.CreateDirectory(runDirectory);
                        _runPath = runDirectory;
                        break;
                    }
                }
            }
        }

        public static void Setup()
        {
            if (_instance == null)
            {
                _instance = new ScreenshotUtil();
            }
        }

        public static ScreenshotUtil Instance
        {
            get
            {
                Setup();
                return _instance;
            }
        }

        public void TakeScreenshot(string appendTitle, int waitMilliseconds = 1000)
        {
            if (string.IsNullOrEmpty(_runPath))
                return;

            //File name format: TestCategory_{appendTitle}.jpg
            var fileName = GenerateFileName(appendTitle);

            var path = string.Format("{0}\\{1}", _runPath, GetFolderName()); 
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            Thread.Sleep(waitMilliseconds);

            var fullPath = string.Format("{0}\\{1}.jpg", path, fileName);            
            Desktop.TakeScreenshot(fullPath, ImageFormat.Jpeg);            
        }

        public void TakeScreenshot(Rect rect, string appendTitle, int waitMilliseconds = 1000)
        {
            if (string.IsNullOrEmpty(_runPath))
                return;

            //File name format: TestCategory_{appendTitle}.jpg
            var fileName = GenerateFileName(appendTitle);

            var path = string.Format("{0}\\{1}", _runPath, GetFolderName());
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            Thread.Sleep(waitMilliseconds);

            var fullPath = string.Format("{0}\\{1}.jpg", path, fileName);
            var bitmap = CaptureArea(rect);
            bitmap.Save(fullPath);
        }

        #region Helpers

        private string GenerateFileName(string appendTitle)
        {
            var frame = GetStackFrame();

            if (frame == null)
            {
                return appendTitle;
            }

            var method = frame.GetMethod();
            var attrs = method.GetCustomAttributes(typeof(TestCategoryAttribute), true).Cast<TestCategoryAttribute>();
            var categoryName = string.Empty;
            foreach (var attr in attrs)
            {
                var categories = attr.TestCategories;
                if (!string.IsNullOrEmpty(categoryName))
                {
                    break;
                }
            }

            var filename = categoryName;
            if (!string.IsNullOrEmpty(appendTitle))
            {
                filename = string.Format("{0}_{1}", filename, appendTitle);
            }
            return filename;
        }

        private string GetFolderName()
        {
            var frame = GetStackFrame();

            if (frame == null)
            {
                return string.Empty;
            }

            var method = frame.GetMethod();

            var attrs = method.GetCustomAttributes(typeof(ScreenshotFolderAttribute), true);
            if (attrs != null && attrs.Count() > 0)
            {
                var attr = (ScreenshotFolderAttribute)attrs.First();
                return attr.FolderName;
            }

            return method.ReflectedType.Name;
        }

        private StackFrame GetStackFrame()
        {
            var stackTrace = new StackTrace();
            var frame = stackTrace.GetFrames().FirstOrDefault(fr => IsTestMethodFrame(fr));
            return frame;
        }

        private bool IsTestMethodFrame(StackFrame stackFrame)
        {
            var method = stackFrame.GetMethod();
            var attr = method.GetCustomAttributes(typeof(TestMethodAttribute), true);
            return (attr.Count() > 0);
        }

        private Bitmap CaptureArea(Rect rect)
        {
            var rectangle = new Rectangle((int)rect.X, (int)rect.Y, (int)rect.Width, (int)rect.Height);
            var width = rectangle.Right - rectangle.Left;
            var height = rectangle.Bottom - rectangle.Top;
            var bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            var graphics = Graphics.FromImage(bmp);

            graphics.CopyFromScreen(rectangle.Left, rectangle.Top, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);

            return bmp;
        }

        #endregion
    }
}
