﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace InstallTests.WindowObjects
{
    public class CancelOkWindow
    {
         private Window _window;

        //Constructor
         public CancelOkWindow(Window window)
        {
            _window = window;
        }

        private Button btnOK
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("OK"));
            }
        }

        public void ClickCancelOK()
        {
             btnOK.Click();
        }

        public bool IsButtonCancelOkExists()
        {
            return btnOK.Visible;
        }
    }
}
