﻿using InstallTests.Extensions;
using System;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace InstallTests.WindowObjects
{
    public class ChangeInstallationWindow
    {
        private Application _application;
        private Window _window;
        private SearchCriteria _repairButton = SearchCriteria.ByText("Repair");
        private SearchCriteria _removeButton = SearchCriteria.ByText("Remove");

        public ChangeInstallationWindow(Application application)
        {
            _application = application;
            GetWindow((win) =>
            {
                return
                    (
                    win.Exists<Button>(_repairButton) &&
                    win.Exists<Button>(_removeButton)
                    );
            });
        }

        private Button btnRepair
        {
            get
            {
                return _window.Get<Button>(_repairButton);
            }
        }

        private Button btnRemove
        {
            get
            {
                return _window.Get<Button>(_removeButton);
            }
        }

        public void ConfirmRepair()
        {
            _window.TakeScreenshot("3-maticChangeRepairWindow");
            btnRepair.Click();
            GetWindow((win) =>
            {
                return
                    (
                    win.Exists<Button>(_repairButton) &&
                    !win.Exists<Button>(_removeButton)
                    );
            });
            _window.TakeScreenshot("RepairWindow");
            btnRepair.Click();
        }

        public void ConfirmRemove()
        {
            _window.TakeScreenshot("3-maticChangeRemoveWindow");
            btnRemove.Click();
            GetWindow((win) =>
            {
                return
                    (
                    !win.Exists<Button>(_repairButton) &&
                    win.Exists<Button>(_removeButton)
                    );
            });
            _window.TakeScreenshot("RemoveWindow");
            btnRemove.Click();
        }

        private void GetWindow(Func<Window, bool> predicate)
        {
            _window = _application.WaitForWindow(predicate);
        }
    }
}
