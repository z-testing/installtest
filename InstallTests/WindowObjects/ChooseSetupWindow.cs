﻿using InstallTests.Extensions;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace InstallTests.WindowObjects
{
    public class ChooseSetupWindow
    {
        private Window _window;
        private SearchCriteria _completeButton = SearchCriteria.ByText("Complete");
        
        #region Button
        private Button btnComplete
        {
            get
            {
                return _window.Get<Button>(_completeButton);
            }
        }

        private Button btnInstall
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Install"));
            }
        }

        private Button btnCustom
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Custom"));
            }
        }

        private Button btnBrowse
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Browse..."));
            }
        }

        private Button btnOK
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("OK"));
            }
        }
        #endregion

        //Init
        public ChooseSetupWindow(Application application)
        {
            _window = application.WaitForWindow((win) =>
            {
                return
                    (
                    win.Exists<Button>(_completeButton)
                    );
            });
        }

        //Refactor to enum
        public ReadyToInstallWindow SelectCompleteInstall()
        {
            btnComplete.Click();
            return Windows.ReadyToInstallWindow;
        }

        public ReadyToInstallWindow SelectCustomInstall()
        {
            btnCustom.Click();
            return Windows.ReadyToInstallWindow;
        }

        public ReadyToInstallWindow StartInstall()
        {
            btnInstall.Click();
            return Windows.ReadyToInstallWindow;
        }

        public ChooseSetupWindow BrowseFileLocation()
        {
            btnBrowse.Click();
            return Windows.ChooseSetupWindow;
        }

        public ChooseSetupWindow ClickOKAfterBrowse()
        {
            btnOK.Click();
            return Windows.ChooseSetupWindow;
        }
    }
}
