﻿using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace InstallTests.WindowObjects
{
    public class CloseApplicationsWarningWindow
    {
        private Window _window;

        public CloseApplicationsWarningWindow(Window window)
        {
            _window = window;
        }

        private Button btnCancel
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Cancel"));
            }
        }

        public void ClickCancel()
        {
            btnCancel.Click();
        }
    }
}
