﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace InstallTests.WindowObjects
{
    public class ConfirmCancelSetupWindow
    {
        private static Application _application;
        private Window _window;

        public ConfirmCancelSetupWindow(Window window)
        {
            _window = window;
        }

        private Button btnYes
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Yes"));
            }
        }

        private Button btnFinish
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Finish"));
            }
        }

        private Button btnOK
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("OK"));
            }
        }


        public ConfirmCancelSetupWindow ClickYes()
        {
            btnYes.Click();
            return Windows.ConfirmCancelSetupWindow;
        }

        public void ClickFinish()
        {
            btnFinish.Click();
        }

        public void ClickOK()
        {
            btnOK.Click();
        }
    }
}
