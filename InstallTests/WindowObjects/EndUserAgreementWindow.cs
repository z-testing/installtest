﻿using InstallTests.Extensions;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace InstallTests.WindowObjects
{
    public class EndUserAgreementWindow
    {
        private Window _window;
        private SearchCriteria _nextButton = SearchCriteria.ByText("Next");
        private SearchCriteria _agreementCheckBox = SearchCriteria.ByText("I accept the terms of this License Agreement");
                
        //Elements
        private CheckBox chbAgreement
        {
            get
            {
                return _window.Get<CheckBox>(_agreementCheckBox);
            }
        }

        private Button btnPrint
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Print"));
            }
        }

        private Button btnBack
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Back"));
            }
        }

        private Button btnNext
        {
            get
            {
                return _window.Get<Button>(_nextButton);
            }
        }

        private Button btnCancel
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Cancel"));
            }
        }

        private Button btnOpenUserAgreement
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByAutomationId("477"));
            }
        }
        private Button btnComplete
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Complete"));
            }
        }

        private Button btnCustom
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Custom"));
            }
        }

        private Button btnBrowse
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Browse..."));
            }
        }

        private Button btnOK
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("OK"));
            }
        }

        private Button btnEnd_User_License_Agreement
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Click here to view our most recent End-User License Agreement EULA"));
            }
        }

        //Init
        public EndUserAgreementWindow(Application application)
        {
            _window = application.WaitForWindow((win) =>
            {
                return
                    (
                    win.Exists<CheckBox>(_agreementCheckBox) &&
                    win.Exists<Button>(_nextButton)
                    );
            });
        }

        public EndUserAgreementWindow AcceptTermsOfUsage()
        {
            chbAgreement.Checked = true;
            return this;
        }

        public ChooseSetupWindow GotoChooseSetupType()
        {
            btnNext.Click();
            return Windows.ChooseSetupWindow;
        }

        public ReadyToInstallWindow SelectCompleteInstallAce()
        {
            btnComplete.Click();
            return Windows.ReadyToInstallWindow;
        }

        public EndUserAgreementWindow SelectCustomInstallAce()
        {
            btnCustom.Click();
            return Windows.EndUserAgreementWindow;
        }

        public EndUserAgreementWindow SelectEndUserLicenseAgreement()
        {
            btnEnd_User_License_Agreement.Click();
            return Windows.EndUserAgreementWindow;
        }

        public EndUserAgreementWindow BrowseFileLocationAce()
        {
            btnBrowse.Click();
            return Windows.EndUserAgreementWindow;
        }

        public EndUserAgreementWindow ClickOKAfterBrowseAce()
        {
            btnOK.Click();
            return Windows.EndUserAgreementWindow;
        }

        public ReadyToInstallWindow ClickNextAfterCustom()
        {
            btnNext.Click();
            return Windows.ReadyToInstallWindow;
        }

        public string CheckWebsiteExists()
        {
            string value = "";
            //Thread.Sleep(TimeSpan.FromSeconds(15));

            //foreach (var process in Process.GetProcesses())
            //{
            //    if (process.ProcessName.StartsWith("iexplore"))
            //    {

            //        SHDocVw.InternetExplorer iexplore = SHDocVw.InternetExplorer;
            //        value = iexplore.LocationURL;
            //        return value;
            //    }
            //}

            return value;
        }
    }
}

