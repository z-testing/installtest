﻿using InstallTests.Extensions;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace InstallTests.WindowObjects
{
    public class FinishSetupWindow
    {
        private Window _window;
        private SearchCriteria _OKButton = SearchCriteria.ByText("OK");

        //Constructor
        public FinishSetupWindow(Application application)
        {
            _window = application.WaitForWindow((win) =>
            {
                return
                    (
                    win.Exists<Button>(_OKButton)
                    );
            });
        }

        private Button btnOK
        {
            get
            {
                return _window.Get<Button>(_OKButton);
            }
        }

        public void ClickOK()
        {
             btnOK.Click();
        }
    }
}
