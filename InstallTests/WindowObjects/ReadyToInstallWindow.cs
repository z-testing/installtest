﻿using InstallTests.Extensions;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace InstallTests.WindowObjects
{
    public class ReadyToInstallWindow
    {
        private Window _window;
        private Application _application;
        private SearchCriteria _installButton = SearchCriteria.ByText("Install");
        private SearchCriteria _finishButton = SearchCriteria.ByText("Finish");
        
        private Button btnInstall
        {
            get
            {
                return _window.Get<Button>(_installButton);
            }
        }

        private Button btnFinish
        {
            get
            {
                return _window.Get<Button>(_finishButton);
            }
        }

        private Button btnBack
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Back"));
            }
        }

        private Button btnCancel
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Cancel"));
            }
        }

        private Button btnNext
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Next"));
            }
        }

        //Init
        public ReadyToInstallWindow(Application application)
        {
            _application = application;
            _window = application.WaitForWindow((win) =>
            {
                return
                    (
                    win.Exists<Button>(_installButton)
                    );
            });
        }

        public ReadyToInstallWindow StartInstall()
        {
            btnInstall.Click();

            _window = _application.WaitForWindow((win) =>
            {
                return
                    (
                    win.Exists<Button>(_finishButton)
                    );
            });
            return this;
        }

        public ReadyToInstallWindow ClickNextAfterCustom()
        {
            btnNext.Click();
            return Windows.ReadyToInstallWindow;
        }

        //Checks
        public bool IsButtonInstallExists()
        {
            return btnInstall.Visible;
        }

        public FinishSetupWindow ClickFinish()
        {
            btnFinish.Click();
            return Windows.FinishSetupWindow;
        }
    }
}
