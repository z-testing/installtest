﻿using System.Linq;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace InstallTests.WindowObjects
{
    public class RestartSystemWindow
    {
        private Window _window;

        public RestartSystemWindow(Application application)
        {
            var windows = application.GetWindows();
            _window = windows.Where(win => win.Enabled).First();
        }

        private Button btnYes
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Yes"));
            }
        }

        private Button btnNo
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("No"));
            }
        }

        public void ClickYes()
        {
            btnYes.Click();
        }

        public void ClickNo()
        {
            btnNo.Click();
        }
    }
}
