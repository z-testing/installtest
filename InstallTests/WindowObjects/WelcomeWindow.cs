﻿using InstallTests.Extensions;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace InstallTests.WindowObjects
{
    public class WelcomeWindow
    {
        private Window _window;
        private SearchCriteria _nextButton = SearchCriteria.ByText("Next");
        private SearchCriteria _cancelButton = SearchCriteria.ByText("Cancel");

        //Init
        public WelcomeWindow(Application application)
        {
            _window = application.WaitForWindow((win) =>
            {
                return
                    (
                    win.Exists<Button>(_nextButton) &&
                    win.Exists<Button>(_cancelButton) &&
                    win.Get<Button>(_nextButton).Enabled &&
                    win.Get<Button>(_cancelButton).Enabled
                    );
            });
        }

        //TODO: create region
        private Button btnBack
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Back"));
            }
        }

        private Button btnNext
        {
            get
            {
                return _window.Get<Button>(_nextButton);
            }
        }

        private Button btnCancel
        {
            get
            {
                return _window.Get<Button>(_cancelButton);
            }
        }

        private TextBox txtTitle
        {
            get
            {
                return _window.Get<TextBox>(SearchCriteria.ByAutomationId("112"));
            }
        }

        private TextBox txtDescription
        {
            get
            {
                return _window.Get<TextBox>(SearchCriteria.ByAutomationId("11"));
            }
        }

        public EndUserAgreementWindow GotoEndUserAgreement()
        {
            btnNext.Click();
            
            return Windows.EndUserAgreementWindow;
        }

        public ChooseSetupWindow GotoChooseSetupType()
        {
            btnNext.Click();
            return Windows.ChooseSetupWindow;
        }

        //TODO: Add Return of new window
        public ConfirmCancelSetupWindow CancelSetup()
        {
            btnCancel.Click();
            return Windows.ConfirmCancelSetupWindow;
        }

        public void GetShortDescription()
        {
            //TODO:Implement
        }

        public void GetDescription()
        {
            //TODO:Implement
        }
    }
}
