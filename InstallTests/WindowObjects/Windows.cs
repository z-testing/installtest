﻿using System;
using System.Linq;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.Utility;

namespace InstallTests.WindowObjects
{
    public static class Windows
    {
        private static Application _application;

        private static Window GetWindow(string windowTitle)
        {
            return Retry.For(
                () => _application.GetWindows().First(x => x.Title.Contains(windowTitle)),
                TimeSpan.FromSeconds(60));
        }

        public static void Init(Application application)
        {
            _application = application;
        }


        //Windows
        public static WelcomeWindow WelcomeWindow
        {
            get
            {
                return new WelcomeWindow(_application);
            }
        }

        public static ReadyToInstallWindow ReadyToInstallWindow
        {
            get
            {
                return new ReadyToInstallWindow(_application);
            }
        }

        public static EndUserAgreementWindow EndUserAgreementWindow
        {
            get
            {

                return new EndUserAgreementWindow(_application);
            }
        }

        public static ChooseSetupWindow ChooseSetupWindow
        {
            get
            {
                return new ChooseSetupWindow(_application);
            }
        }

        public static FinishSetupWindow FinishSetupWindow
        {
            get
            {
                return new FinishSetupWindow(_application);
            }
        }

        public static ConfirmCancelSetupWindow ConfirmCancelSetupWindow
        {
            get
            {
                return new ConfirmCancelSetupWindow(_application.GetWindow(_application.Process.MainWindowTitle));
            }
        }

        public static CancelOkWindow CancelOkSetupWindow
        {
            get
            {
                var windowsTitle = _application.Process.MainWindowTitle.ToString();
                return new CancelOkWindow(
                    _application.GetWindow(
                   windowsTitle.Substring(0, windowsTitle.Length - 6)));
            }
        }
    }
}
