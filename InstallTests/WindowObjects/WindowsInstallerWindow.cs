﻿using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace InstallTests.WindowObjects
{
    public class WindowsInstallerWindow
    {
        private Window _window;

        public WindowsInstallerWindow(Application application)
        {
            _window = application.GetWindow("Windows Installer");
        }

        private Button btnYes
        {
            get
            {
                return _window.Get<Button>(SearchCriteria.ByText("Yes"));
            }
        }

        public void ClickYes()
        {
            btnYes.Click();
        }
    }
}
